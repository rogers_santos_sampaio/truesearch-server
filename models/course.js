var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var course = new Schema({
    name: String,
    teachers: [String],
    duration: String,
    level: String
});

course.index({ name: 'text', teachers: 'text' });

module.exports = mongoose.model('Course', course);
