'use strict';

var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require('cors');
var fs = require('fs');

var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');

// Constants
var PORT = 8080;

// Mongo
var db = mongoose.connect('mongodb://mongo/CourseAPI');
var Course = require('./models/course');

// App
var app = express();
app.use(cors());

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

// seed DB
var rawdata = fs.readFileSync('./seed/db.json');
var seed = JSON.parse(rawdata);
var doSeed = require('./seed/seed.js')(seed);

// Api Routes
var courseRoutes = require('./routes/courseRoutes')(Course);

app.use('/api/courses', courseRoutes);

// Home page
app.get('/', function(req, res) {

  res.send('Welcome to the TrueSearch API\n');
});

// API documentation
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Start Server
app.listen(PORT, function() {
  console.log('Running on:' + PORT);
});