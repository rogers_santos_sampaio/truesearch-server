var express = require('express');

var routes = function(Course) {

    var apiRouter = express.Router();
    apiRouter.route('/')
    .post(function(req, res) {
        var course = new Course(req.body);
        course.save();
        res.status(201).send(course);
    })
    .get(function(req, res) {
        var query = req.query.search;

        if (!query) {

            Course.find(function(err, courses) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(courses);
                }
            });
            
        } else {

            Course.find({ $text: { $search: query } }, function(err, courses) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(courses);
                }
            });

        }
       
    });

    apiRouter.route('/:id')
        .get(function(req, res) {
            Course.findById(req.params.id, function(err, courses) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(courses);
                }
                });
        });

    return apiRouter;
};

module.exports = routes;