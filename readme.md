# Truesearch, a real time suggestion search api

## Purpose
This project is intented to display my coding habilities for the Schoology Software Enginnering position.

## Instructions
This is a nodejs and express API server. It uses a MongoDB docker instance to store the data.
This project has one Dockerfile and on Docker Compose file follow the instructions to run the server:

```docker-compose up```

The server will map itself to the port 80, but it can be changed in the docker composer file line 8

I created a Digital Ocean Droplet where I am running docker and it can be found here:

```http://206.189.231.167/```

There is a swagger api documentation at:

```http://206.189.231.167/api-docs/```

There you will be able to see all the endpoints created and the model used, as well will be able to test them.